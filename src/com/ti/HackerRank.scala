import scala.annotation.tailrec

/**
  * Created by tomasz on 6/3/2017.
  */

object Solution {

 // def f(n: Int) = (1 to n).foreach{ i => println("Hello World") }

  // Display Hello World n times
  //val f = (n:Int) => (1 to n).foreach{ i => println("Hello World") }// function

  // duplicate each numer in list num of times
  //def f(num:Int,arr:List[Int]):List[Int] = arr.flatMap(i =>  List.fill(num)(i))
  //def g(x:Int) = x + 1 // method

  // filter out values less than delim
  //def f(delim:Int,arr:List[Int]):List[Int] = arr.filter(i => i < delim)
 //remove the elements at odd positions
  //def f(arr:List[Int]):List[Int] = arr.zipWithIndex.filter(p => p._2 % 2 == 1).map(p => p._1)

  // Reverse list withou using reverse metho on list
  //def f(arr:List[Int]):List[Int] = arr.zipWithIndex.sortWith((a,b) => a._2 > b._2).map(p => p._1) /*arr.reverse*/
  // sum of odd elements
  //def f(arr:List[Int]):Int = arr.filter(i => i % 2 == 1 || i % 2 == - 1).sum
  // Count the number of elements in an array without using count, size or length operators
  //def f(arr:List[Int]):Int = arr.map(i=> (i,1)).map(_._2).foldLeft(0) ((total:Int,next:Int) => total + next)
  // Update the values of a list with their absolute values
  //def f(arr:List[Int]):List[Int] = arr.map(scala.math.abs(_))


  //https://www.hackerrank.com/challenges/eval-ex?h_r=next-challenge&h_v=zen

  /*def factorial(n: Double) = {
    // Function within function
    @tailrec /// it will make enforce that you using tail recursion
    def tailFactorialImpl(n: Double, fact: Double): Double = {
      if (n == 0 || n == 1)
        fact
      else
        tailFactorialImpl(n - 1, n * fact) // tail call
    }

    tailFactorialImpl(n, 1d)
  }
  def series(x:Double,n: Double) = scala.math.pow(x,n)/factorial(n)

  def seriesExpansion (x:Double) =  1 + x + series(x,2) + series(x,3) +  series(x,4) +  series(x,5)  +  series(x,6) +  series(x,7) +  series(x,8) +  series(x,9)

  def f(arr:List[Double]):List[Double] = arr.map(x=> seriesExpansion(x))


  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in)
    var n = sc.nextInt()
    var a0 = 0
    while(a0 < n){
      var x = sc.nextDouble()
      println(seriesExpansion(x))
      a0+=1
    }
  }*/


//  def pointsToVectors(head:(Double,Double),tail:List[(Double,Double)], result:List[((Double,Double),(Double,Double))]) = {
//    // Function within function
//    @tailrec
//    def pointsToVectors(head:(Double,Double),tail:List[(Double,Double)],result:List[((Double,Double),(Double,Double))]): Unit = {
//     // if(!tail.isEmpty)
//       // result += (head,tail.head)
//
//      pointsToVectors(tail.head, tail.tail, result)
//    }
//    pointsToVectors(tail.head, tail.tail, result)
//  }

  def area(vectors:List[((Double,Double),(Double,Double))]):Double = {
    // x1 = points._1._1
    // y1 = points._1._2
    // x2 = points._2._1
    // y2 = points._2._2

    val sums:List[Double] =  vectors.map(vector =>  vector._1._1 *  vector._2._2 - vector._2._1 *  vector._1._2)
    0.5 * sums.foldLeft(0d)  { (total:Double,next:Double) => total + next }

  }
  def distance(p1:(Double,Double),p2:(Double,Double)):Double = scala.math.sqrt(scala.math.pow(p2._1 - p1._1,2)  + scala.math.pow(p2._2 - p1._2,2))


  def perimeter(vectors:List[((Double,Double),(Double,Double))]):Double = {
    vectors.map(v1=> distance(v1._1, v1._2)).foldLeft(0d) ( _ + _)
  }

  def main(args: Array[String]) {

   // val points: List[(Double, Double)] = readInputParams

       val points = List[(Double,Double)] ((1043,770),(551,551),(681,463))
// //   val points = List[(Double,Double)] ((0,0),(1,1),(2,2),(2,0))
  //  val points = List[(Double,Double)] ((0,0),(0,1),(1,1),(1,0))
//    val points = List[(Double,Double)] ((0,0),(0,2),(2,2),(2,0))

//    3
//    1043 770
//    551 990
//    681 463
//    1556.3949033


//    8
//    458 695
//    621 483
//    877 469
//    1035 636
//    1061 825
//    875 1023
//    645 1033
//    485 853
//   1847.48055065
    val vectors:List[((Double,Double),(Double,Double))] = buildVector(points)
    //println(perimeter(vectors))
    println(area(vectors))
  }

  private def readInputParams = {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt()
    var a0 = 0
    val b = List.newBuilder[(Double, Double)]

    while (a0 < n) {
      a0 += 1
      b += Pair(sc.nextDouble(), sc.nextDouble())
    }

    val points = b.result
    points
  }

  private def buildVector(points: List[(Double, Double)]) = {
    points.zip(buildSecondPoints(points.head, points.tail))
  }

  private def buildSecondPoints(firstElement: (Double, Double), restOfElements: List[(Double, Double)]) = {
    restOfElements ::: List(firstElement)
  }
}