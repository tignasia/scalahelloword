package com.ti

import scala.concurrent.{ExecutionContext, Future}


class Departament(name:String, parent: Option[Departament]) {
  def this(name:String) = this(name,None)
  override def toString: String = s"$name,$parent"
  def foo(x:Int):Unit = println(x +1)
  def foo2(x:Int) {
    println(x +1)
  }

  def apply(x:Int) = x + 1

}

//case class creates apply toString equals hashcode and ccopy method
case class Employee(val firstName:String, val lastName:String, val departament:Departament) {

  // Precondition checks - part of default constructor
  require(firstName.nonEmpty, "First name cannot be empty !")
  require(lastName.nonEmpty, "First name cannot be empty !")
  require(departament != null , "First name cannot be empty !")

  // Alternative secondary constructor with filled in default param
  def this(firstName:String, lastName:String) = this(firstName,lastName,new Departament("Dupa"))

  def fullname = s"$firstName $lastName"
}

class Manager (override  val firstName:String, override  val lastName:String, override  val departament:Departament)
  extends Employee(firstName, lastName, departament)   {

  // override keyword must be used
  override def fullname: String = /*super.fullname*/ s"$firstName $lastName ($departament)"

}

object Employee {
 // static factories / apply to construct
  // utility methods
  // pattern matching
  // implicit recepies
  def apply(firstName:String, lastName:String) :Employee = this(firstName,lastName,new Departament("Dupa"))
}

class Foo(x:Int) {
  // operator overloading
  def ~(y:Int) = x + y
  def ~:(y:Int) = x + y // colon at the end - right associative - PREPEND
}

// singleton
object Explore2 {

  // method static because it is on object
  def foo(x:Int)= { x +1 }

  def fillLisOfTuples(x:Tuple2[Int,String]) = List.fill(x._1)(x._2)


  def processNumbers(b:Boolean = true, x:Int, y:Int) = if(b) x else y

  def add(x:Int, y:Int = 10) = x + y

  def main(args: Array[String]): Unit = {

    println("Hello Scala !")

    val dep = new Departament("RFIT")
    val emp = Employee("Tomasz", "Ignasiak", dep)
    println(emp)
    println(emp.departament.foo(10))
    println(foo(10))

    val singletonRef =  Explore2
    println(singletonRef)
    println(singletonRef == Explore2)
    dep(1)
    val emp2 = Employee("Tomasz", "Ignasiak")
    println(emp2)

    val list =  List(1,2,3,4) // List ( companion object) .apply()

    val f = new Foo(10)
    println(f.~(5))
    println(f ~ 5)

    println( 12 + 16)
    println( 12.+(16))

    println(f.~:(40))
    println(40 ~: f)

    println(Nil) // Nil = EMPTY LIST !!!!
    //Nothing
    val  listOfNothing/*:List[Nothing]*/ = List() // Nothing is subtype of everything !
    println(listOfNothing) //

    // prepend 3 to empty list
    val list2 = 3 :: Nil
    println(list2)
    // prepend 3 to empty list and the prepend 2
    val list3 = 2 :: 3 :: Nil
    println(list3)

    // +:  - prepend operator on list
    // :+ - append operator on list
    println(0+: List(1,2,3) :+ 5)


    val list4  = (1 to 10).toList
    println(list4)

    println(list4.updated(3,100)) // "replaces" 3rd element with 100 - in reality make immutable copy


    println(list4 ++ (11 to 20).toList)

    val dep2 = new Departament("MyDep",Some(new Departament("ParentDep")))
    println(dep2)

    // tuples
    val tuple = (2, "Foo")
    println(tuple)
    val listOFTuples = fillLisOfTuples((4,"Bob"))
    val listOFTuples2 = fillLisOfTuples(4 -> "Bob") // same
    println(listOFTuples)
    println(listOFTuples2)

    val map = Map(1 -> "One", 2 -> "Two",  3 -> "3") // -> is Method
    println(map)
    println(map.get(1))
    println(map.get(2))
    println(map.get(3).getOrElse(0))
    println(map.get(5).getOrElse(0))

    val vector = Vector(1,2,3,4) // FAST
    println(vector)
    val listOFCharacters = "Hello World".toList
    println(listOFCharacters)
    println(listOFCharacters.toSet)


    // Apply must be implemented
    val function = new Function1[String,Int] {
      override def apply(s: String): Int = s.length
    }
    // same
    val function2 = (s: String) => s.length

    // same
    val function3:String => Int = s => s.length

    // same
    val function4:String => Int = _.length



    // Us of Funtion1 (one input param, one output)
    println(List("Ala","Ma","Kota").map(function).toList)

    // Use clousure instead
    println(List("Ala","Ma","Kota").map( s => s.length).toList)


    // same
    println(List("Ala","Ma","Kota").map(function2).toList)

    println(List("Ala","Ma","Kota").map(function3).toList)

    println(List("Ala","Ma","Kota").map(function4).toList)


    println(List("Ala","Ma","Kota").filter( e => e.length >= 3))
    println(List("Ala","Ma","Kota").filter(_.length >= 3))

    val listOfList:List[List[Int]] = List(1,2,3,4).map( x=> List(-x,x,x+1))
    println(listOfList)
    println(listOfList.flatten)
    val listOfList2:List[Int] = List(1,2,3,4).flatMap( x=> List(-x,x,x+1))
    println(listOfList2)

    val lyrics = List("I see trees of green", "Red roses too","I see them bloom", "for me and you", "and I think to myself", "What a wonderful world")

    val lyricsWords = lyrics.flatMap(s => s.split("\\W+"))
    println(lyricsWords)
    val lyricsWordsGroupdedBy1stLetter:Map[Char,List[String]] = lyricsWords.map( w => w.toLowerCase).groupBy(w => w.head)
    println(lyricsWordsGroupdedBy1stLetter)
    val lyricsWordsGroupdedByWord:Map[String,List[String]] = lyricsWords.map( w => w.toLowerCase).groupBy(w => w)
    println(lyricsWordsGroupdedByWord)
    val wordCount:Map[String,Int]  = lyricsWordsGroupdedByWord.mapValues(ls => ls.size)
    println(wordCount) // word count
    println(wordCount.get("and"))
    lyrics.foreach( s => println(s))

    val x = 10

    val result = if (x > 10) "Foo"  else "Bar" // if assigned to value


    import ExecutionContext.Implicits.global
    //implicit val  executionContext = ExecutionContext()


    // TODO - dosent work
    val fut = Future {
     // Thread.currentThread().setDaemon(true)
      Thread.sleep(10000)
      println(Thread.currentThread().getName)
      40 + 100
    }

   fut.map(i => i  * 20).foreach(println)


    // standard for loop (imperative style)
    var res  = ""
    for (a <- 1 to 100) {
      res = res + a
      if(a < 100)
        res = res + ","
    }
    println(res)




    val xs = List(1,2,3,4)
    var resultXs = List[Int]()

    for( a <- xs) {
      resultXs = resultXs :+ (a + 1)
    }

    println(resultXs)

    // Real for comprehension - yield says what value to return for each element in for loop (similar to map)
    val resultXs2 = for( a <- xs) yield  (a + 1)

    println(resultXs2)



    // Named parameters
    println(processNumbers(true,10,41))

    println(processNumbers(x = 10, y = 41, b =true))

    add(4,3)
    // Default arguments
    add(30)

    println(processNumbers(x = 10, y  =41)) // if default ar is not last named params must be used


   // On Any we have have isInstanceOf and asInstanceOf
    println(3.isInstanceOf[Int])
    println(3.isInstanceOf[Boolean]) // java istanceof


    val g:Any = "Ala mo kota"
    val h:String = g.asInstanceOf[String] // no cast in scala
    // use
    // if (x.isInstanceOf[X]) x.asInstanceOf[x] as equvalent of java's
    // if (x instanceof X) return (X)x
    try {
      val emp3 = new Employee("Ala", "")
    } catch {
      // patter matching on type
      case iae:IllegalArgumentException => println(iae.getMessage)
    } finally {
      println("Continuing ...")
    }

    val emp4 = Employee("Ala", "Krala") // apply from case class
     println(emp4.copy(lastName = "Lala"))
    val name = emp4 match  {
      case Employee(f,l,d) => (f,l) // unapply defined on case class
      case _ => ("Unknown","Unknown")
    }
    println(name)
  }






}

object Runner extends App {
  println("Hello World from Scala Runner !!")

}

//case class Departament()

//class Explore2 {
//
//}
