package com

/**
  * Package object are good way to expose API
  */
package object ti {

 case class Order(currencyPair: (String,String), amount : Double )

 class TradingApi {
    def  placeOrder(order : Order) {}
 }
  def getTradingApi(broker: String) : TradingApi = {???}
}
