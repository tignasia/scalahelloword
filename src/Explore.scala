
//import java.util

import java.io.File._
import java.io.{BufferedInputStream, File, FileInputStream}
import java.time.LocalDate
import java.util

import Lists.a

import scala.annotation.tailrec
import scala.beans.BeanProperty
import scala.collection.mutable
//import java.util.{Map, HashMap}


/**
  * Created by tomasz on 1/10/2016.
  */

// Class andConstructor defined together
// case class implements constructor, geters/setters for proerties equals, hashcode, toString , apply (construction) , unapply (deconstruction used e.g. in pattern matching)
case class Customer(val name: String, postcode: String = "", var _address: String = "", val total: Double) {
  private val id = Customer.nextId()

  // overidding of address setter method
  def address = _address

  def address_=(newAddress: String) {
    println("New Address = " + newAddress)
    _address = newAddress.toUpperCase
  }

}

// Static "friend" class

object Customer {
  private var seqOfIds: Int = 0

  private def nextId(): Int = {
    seqOfIds += 1
    seqOfIds
  }


  def main(args: Array[String]): Unit = {
    // apply calling Customer without new calls appy method
    val customer = Customer("Jausz Wspanialy", "36-223", "Walkowice gorne", 0d)
    println(s"${customer.id} = ${customer}")
    customer.address = "Dupowice Dolne"
    println(s"${customer.id} = ${customer}")
    // When using optional/default values prameter names must be explicitly stated
    val customer2 = new Customer(name = "Tomasz Mak", _address = "Walkowice gorne", total = 0d)
    println(s"${customer2.id} = ${customer2}")
  }

}

case class SimplePerson(lastname: String, firstname: String, birthYear: Int)

object SimplePerson {

  // Factory function using apply()
  val personCreator: (String, String, Int) => SimplePerson = SimplePerson.apply _
  val personCreator2: (String, String, Int) => SimplePerson = {
    SimplePerson.apply
  }

  def main(args: Array[String]): Unit = {
    val p = SimplePerson("Lacava", "Alessandro", 1976)
    println(p)
    // Copy method
    val pCopy = p.copy(lastname = "Dupa", birthYear = 1986)
    println(pCopy)
    //  Shows unapply in action
    val SimplePerson(lastname, _, _) = p
    println(lastname)
    val p2 = personCreator("Brown", "John", 1969)
    println(p2)
    val p3 = personCreator2("Tomasz", "Ignasiak", 1973)
    println(p3)
  }
}

// currying process transforms a function of two parameters into a function of one parameter which returns a function of one parameter which itself returns the result.
// More for curried functions - https://gleichmann.wordpress.com/2011/12/04/functional-scala-curried-functions-and-spicy-methods/
object CurryingAndUnderscores {

  // Add defined as method def
  def add(x: Int, y: Int) = x + y

  // Add defined as function assigned to const
  // val add2: (Int, Int) => Int = (x:Int, y:Int) => x + y
  // Shorcut for above using using _ + _
  val add2: (Int, Int) => Int = _ + _


  // after currying
  def add(x: Int) = (y: Int) => x + y

  // same as above written differently
  def add3(x: Int)(y: Int) = x + y


  val numbers = Array(1, 2, 3, 4, 5)
  // sum by reduce left
  //val sum = numbers.reduceLeft((a:Int, b:Int) => a + b)
  val sum = numbers.reduceLeft(_ + _)

  def main(args: Array[String]): Unit = {

    println(add(1, 7))
    println(add(3, 4))
    println(add2(1, 7))
    println(add2(3, 4))

    println(add(1))
    println(add(3))
    println(add(1)(7))
    println(add(3)(4))


    println(add3(1)(7))
    println(add3(3)(4))

    println(sum)

  }
}

// Faking Funtion Calls via apply and update

class Directory {
  private val numbers = mutable.Map(
    "Tomasz" -> "505 722 195",
    "Joanna" -> "505 686 588",
    "Bartek" -> "701 899 189"
  )


  def apply(name: String) = {
    numbers.get(name)
  }

  def update(name: String, number: String) = {
    numbers.update(name, number)
  }

  def update(areaCode: Int, newAreaCode: String) = {
    // map entry as single variable (Tuple) _1 is key, _2 is value (theyare method calls)
    numbers.foreach(entry => {
      if (entry._2.startsWith(areaCode.toString))
        numbers(entry._1) = entry._2.replace(areaCode.toString, newAreaCode)
    })
  }

  //  override def toString = numbers.toString() {
  //  }

}

object Directory {
  def main(args: Array[String]) {
    val yellowPages = new Directory()
    println("Tomasz's number : " + yellowPages("Tomasz")) // Directory.apply() is called - it is like fake funtion call

    yellowPages(505) = "555" // custom assign operator - call update method
    println(yellowPages.numbers)

    yellowPages("Tomasz") = "Unlisted" // nextfale callto update method
    println("Tomasz's number : " + yellowPages("Tomasz")) // Directory.apply() is called - it is like fake funtion call
  }
}


object FakingLanguageConstructs {
  def main(args: Array[String]) {
    // In meethod call where you pass one argument you canuse curly braces instead of parentheses
    val numerals = List("I", "II", "III", "IV")
    //only method call and not language construct !!!
    numerals.foreach {
      println(_)
    }
  }
}

// HIGER ORDER FUNTIONS
case class ShopingBasket(val name: String, val items: List[String]) {
}

case class OurCustomer(val firstName: String, val secondName: String) {
}


class Ui(basket: ShopingBasket, ourCustomer: OurCustomer) {

  def updateElements(): Unit = {
    new Thread() {
      override def run(): Unit = {
        updateCustomerBasket(basket);
        println(s"Customer ${ourCustomer}")
      } // assign method to method
    }.start()

    new Thread() {
      override def run(): Unit = updateOffersFor(ourCustomer)
    }.start()
  }

  def updateCustomerBasket(basket: ShopingBasket) {
    println(s"Customer ${ourCustomer} : basket updated to ${basket} ")
  }

  def updateOffersFor(consumer: OurCustomer) {
    println(s"Offers to customer ${ourCustomer} updated !")
  }

  // Higher order funtions (takesfuntion as parameter, lambda expression
  def runInThread(function: => Unit): Unit = {
    // () - empty parameter list can be dropped
    new Thread() {
      override def run(): Unit = function // assign method to method --CALLED HERE
    }.start()
  }


  // Curreied vrdion - changing multiarguments fntion to serrise of single argument funtions
  def runInThreadGroup(group: String)(function: => Unit): Unit = {
    // () - empty parameter list can be dropped
    new Thread(new ThreadGroup(group), new Runnable {
      override def run(): Unit = function
    }).start()
  }
}

object Ui {
  def main(args: Array[String]): Unit = {

    val ourCustomer = new OurCustomer("Tomasz", "Ignasiak")
    val ui = new Ui(new ShopingBasket("basket", List("a", "b", "c")), ourCustomer)

    // ALTERNATIVE using higher order funtion runInThread !!

    //lambda expression
    ui.runInThread(() => ui.updateCustomerBasket(new ShopingBasket("basket", List("a", "b", "c", "d"))))
    ui.runInThread(() => ui.updateOffersFor(ourCustomer))

    // or even better - dropping lambda syntax and replacing () with {}

    // CALL BY NAME PARAMETER - NOT CALLED HERE
    ui.runInThread {
      ui.updateCustomerBasket(new ShopingBasket("basket", List("a", "b", "c", "d"))) // Only DEFINED HERE !!!
    }
    ui.runInThread {
      ui.updateOffersFor(ourCustomer) // Only DEFINED HERE !!!
    }


    // HELPcurried funtion
    ui.runInThreadGroup("Basket ") {
      ui.updateCustomerBasket(new ShopingBasket("basket", List("a", "b", "c", "d"))) // Only DEFINED HERE !!!
    }

    ui.runInThreadGroup("Customer") {
      ui.updateOffersFor(ourCustomer) // Only DEFINED HERE !!!
    }

  }
}


class PatternMatching {
  def simpleLiteralMatch(language: String): Unit = {
    language match {
      case "german" => println("Guten Moorgen")
      case "polish" => println("Witaj")
      case "french" => println("Bonjour")
      case _ => println("Hi")
    }
  }
}

object PatternMatching {
  def main(args: Array[String]) {
    val pm = new PatternMatching()
    pm.simpleLiteralMatch("aaa")
    pm.simpleLiteralMatch("polish")
    pm.simpleLiteralMatch("german")
    pm.simpleLiteralMatch("french")

  }
}

// pattern matching on obects
class Person(val name: String)

case class SuperHero(heroName: String, alterEgo: String, powers: List[String]) extends Person(alterEgo)


object SuperHero {
  def main(args: Array[String]) {
    //  val hero = new Person("Joe Sixpack")
    val hero = new SuperHero("Batman", "Bruce Wane", List("Speed", "Power"))
    hero match {
      case SuperHero(_, "Bruce Wane", _) => println("I'm Batman") // works only on case classes by default - calls method unapply
      case SuperHero(_, _, _) => println("???")
      case _ => println("I'm civilian")
    }
  }
}


object LazyVal {
  def main(args: Array[String]): Unit = {
    var divisor = 0
    /*lazy*/ val qutient = 40 / divisor // assigned but not evaluated - evaluated when executed
    // println(qutient) division by zero
    divisor = 2
    println(qutient) // without val it throws ArithmeticException: / by zero
  }
}


object VariblesAndValues {
  def main(args: Array[String]): Unit = {
    val `my most expensive wine` = 35 // any text in backticks
    println(`my most expensive wine`)
    val strangeval_? = 12 // any unicode after _
    println(strangeval_?)
    val śżćńźżó = 24
    println(śżćńźżó)
    val kwiecień = 4
    val b: Byte = 127 // max

    println(b)
    println(1 + 4)
    println(1.+(4))
    println(-5.abs) // Math.abs()

  }
}

object IfStatements {
  def main(args: Array[String]): Unit = {
    val a = 10
    // if assigned to val to avoid var !!!!
    val result = if (a < 10)
      " < 10 "
    else if (a > 10)
      " > 10"
    else {
      " = 10"
    }

    println(result)

  }
}

object WhileLoops2 {
  def main(args: Array[String]): Unit = {
    def traditionalWhile = {
      var a = 100
      var result = ""
      while (a > 0) {
        result = result + a
        if (a > 1)
          result = result + ","

        a = a - 1
      }
      result
    }

    println(traditionalWhile)

    def functionalLoop = {
      (1 to 100).reverse.mkString(",")
      // Same
      //  (100 10 1 by -1).reverse.mkString(",")
    }

    println(functionalLoop)


    def traditionalDoWhile = {
      var a = 100
      var result = ""
      do {
        result = result + a
        if (a > 1)
          result = result + ","

        a = a - 1
      } while (a > 0)
      result
    }

    println(traditionalDoWhile)

  }
}

object ForLoopsAndForComprhensions {
  def main(args: Array[String]): Unit = {
    def forLoop = {
      var result = ""
      for (a <- 1 to 100) {
        result = result + a
        if (a < 100)
          result = result + ","

      }
      result
    }

    // println(forLoop)


    def forLoop2 = {
      val xs = List(1, 2, 3, 4)
      var result = List[Int]()
      for (a <- xs) {
        // overloded operator :+
        result = result :+ (a + 1)
      }
      result
    }

    println(forLoop2)

    def forComprhension = {
      val xs = List(1, 2, 3, 4)
      val result = for (a <- xs) yield (a + 1) // for comprehension  + yield (applies wht in bracets to all items of the list)
      result
    }

    println(forComprhension)
  }

}

object SmartStringsAndStringInterpolation {

  def main(args: Array[String]): Unit = {
    val lyrics =
      """I see trees or green
         @Red roses toom
         @I see then bloom""".stripMargin('@') // trip marins
    println(lyrics)


    val message = "We re meeting on June 13th of this year, and having wounch at 12:30PM"
    val regex = """(\s[0-9])?[0-9]:[0-5][0-9]\s*[AM|PM]""".r // triple quotes- no need to escape characters, r method creates regex
    println(regex.findAllIn(message).toList)

    val str = "This is a %s".format("Test")
    println(str)

    // String interpolation  (s)
    val test = "Test"
    val str2 = s"This is a ${test}"
    // String interpoltion f - like in printf

    val bandname = "Rammstein"
    val tickectCost = 50
    //  f interpolation like printf  %s - ormtted as string, %1.2f  float wuth number of decimal places $ is escape by double $$, %n is new line and % is escape by %
    val percent = 20
    println(f"The $bandname%s tickets are are probably $$$tickectCost%1.2f%nThat is $percent%% increase because everybody loves them")
    // same as smart string
    println(
      f"""The $bandname%s tickets are are probably $$$tickectCost%1.2f
         |That is $percent%% increase because everybody loves them""".stripMargin)
  }
}


object Explore {

  def log(string: String) {
    printf("%s%n", string)
  }

  def main(args: Array[String]) {
    val tomasz = new Customer("Tomasz", _address = "Oborniki Slaskie", total = 99D)
    val joanna = new Customer("Joanna", "Oborniki Slaskie", "55-120", total = 88D)
    //  tomasz.id = "1"
    println(tomasz.address)
    tomasz.address = "AAA"
    println(tomasz.address)
    Explore.log("Finished !")
  }

}


class Mappable[A](elements: A*) {

  def map[B](f: A => B): List[B] = {
    val result = mutable.MutableList[B]()
    elements.foreach {
      result += f.apply(_)
    }
    result.toList
  }
}

object MapExample {
  def main(args: Array[String]) {
    // build-in collections  map
    val numbers2 = List(1, 2, 3, 5)
    println(numbers2.map(_ * 2))

    val numbers = List(1, 2, 3, 5)
    val mapable = new Mappable[Int](numbers: _*) // WTF ?
    val reslts = mapable.map(/*element => element * 2*/ _ * 2)
    println(reslts)

  }


}

//}


// Using Optional -> Some and None are subclasses
trait Customers extends mutable.Iterable[Customer] {
  def add(customer: Customer)

  def find(name: String): Option[Customer]
}


class CusromersSet extends Customers {
  val customers = mutable.Set[Customer]()

  def find(name: String): Option[Customer] = {
    //    customers.foreach { customer =>
    //      if (customer.name == name)
    //        return Some(customer)
    //    }

    for (customer <- customers) {
      if (customer.name == name) // == is equals in Scala ?
        return Some(customer)
    }
    None
  }

  def add(customer: Customer): Unit = customers.add(customer)

  def iterator: Iterator[Customer] = customers.iterator
}

object CusromersSetExample {
  def main(args: Array[String]): Unit = {
    val customers = new CusromersSet()
    val customer1 = Customer("Janusz", "36-223", "Walkowice gorne", 125.55d)
    customers.add(customer1)
    println(customers)
    val basketValue: Option[Double] = customers.find("Albert").map(customer => customer.total)
    println(basketValue)
    val basketValueWithDefultZero: Double = basketValue.getOrElse(0D)
    println(basketValueWithDefultZero)
    val basketValue2: Option[Double] = customers.find("Janusz").map(customer => customer.total)
    println(basketValue2)

    val usCustomers = Set(Customer(name = "Albert", total = 21d), Customer(name = "Beatriz", total = 12d), Customer(name = "Carol", total = 7d))
    val sumOfAllUsClientsBaskets = usCustomers.map(_.total).sum

    val usCustomersOpt = Set(Some(Customer(name = "Albert", total = 21d)), None, Some(Customer(name = "Beatriz", total = 12d)), Some(Customer(name = "Carol", total = 7d)))
    println(sumOfAllUsClientsBaskets)
    val sumOfAllUsClientsBasketsOpt = usCustomersOpt.flatten.map(_.total).sum
    println(sumOfAllUsClientsBasketsOpt)
  }
}


class TestClass(val test: String, val name: String, val value: Integer) {
  override def toString = test + "," + name + "," + value

  //  private final val test: String = null
  //  private final val name: String = null
  //  private final val value: Integer = null
  //
  //  def this(test: String, name: String, value: Integer) {
  //    this()
  //    this.test = test
  //    this.name = name
  //    this.value = value
  //  }
  //
  //  def getTest: String = {
  //    return test
  //  }
  //
  //  def getName: String = {
  //    return name
  //  }
  //
  //  def getValue: Integer = {
  //    return value
  //  }
}

object CalJava {

  def main(args: Array[String]) {
    val testClassJava = new TestClassJava("A", "B", 1)
    println(testClassJava)
    val testClassScala = new TestClass("A", "B", 1)
    println(testClassScala)
  }

}


object TailRecusrsion {
  def factorial(n: Int): Int = {
    if (n == 1)
      1
    else
      n * factorial(n - 1)
  }


  def tailFactorial(n: BigInt) = {
    // Function within function
    @tailrec /// it will make enforce that you using tail recursion
    def tailFactorialImpl(n: BigInt, fact: BigInt): BigInt = {
      if (n == 0 || n == 1)
        fact
      else
        tailFactorialImpl(n - 1, n * fact) // tail call
    }

    tailFactorialImpl(n, BigInt(1))
  }

  //yields a "java.lang.StackOverflowError" with large lists
  // @tailrec
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case x :: tail => x + sum(tail)
  }

  // (2) tail-recursive solution
  //@tailrec
  def sum2(ints: List[Int]): Int = {
    //@tailrec
    def sumAccumulator(ints: List[Int], accum: Int): Int = {
      ints match {
        case Nil => accum
        case x :: tail => sumAccumulator(tail, accum + x)
      }
    }

    sumAccumulator(ints, 0)
  }


  def fib1(prevPrev: Int, prev: Int) {
    val next = prevPrev + prev
    println(next)
    if (next > 1000000)
      System.exit(0)
    fib1(prev, next)
  }


  def fib(x: Int): BigInt = {
    @tailrec
    def fibHelper(x: Int, prev: BigInt = 0, next: BigInt = 1): BigInt = x match {
      case 0 => prev
      case 1 => next
      case _ => fibHelper(x - 1, next, (next + prev))
    }

    fibHelper(x)
  }

  def main(args: Array[String]) {
    //println(factorial(50000)) // StackOverflow
    println(tailFactorial(500))
    //    println(sum2(List(1, 2, 12, 21, 33)))
    //    //println(fib1(1, 2))
    //    println(fib(9))

  }
}

object FunnyMethodNames {
  def `Summation of`(x: Int, y: Int) = x + y

  def main(args: Array[String]): Unit = {
    // Summation of
    `Summation of`(6, 17)
  }
}


object OpratorOverloading {
  def +(x: Int, y: Int) = x + y

  def ~~+:>(x: String) = x.size

  def main(args: Array[String]): Unit = {
    println(~~+:>("Ala Ma Kota"))
    println(3.+(10))
  }
}

object NamedAndDefaultMethodArguments {

  def processNumbers(b: Boolean = true, x: Int, y: Int) = if (b) x else y

  def add(x: Int, y: Int = 10) = x + y // default value

  def main(args: Array[String]): Unit = {
    println(processNumbers(true, 12, 5))
    println(processNumbers(false, 12, 5))
    // named arguments
    println(processNumbers(x = 12, y = 5, b = true))
    println(add(5))
    // Does not work with default not as last arg
    //println(processNumbers(12,5))
    // You have to use default value together with named arguments
    println(processNumbers(x = 12, y = 5))
  }
}

object InstanceOf {

  def decide(x: Any) = {
    if (x.isInstanceOf[Int])
      x.asInstanceOf[Int] + 1
    else
      -1
  }

  def main(args: Array[String]): Unit = {
    println(3.isInstanceOf[Int])
    println(3.isInstanceOf[Double])
    println("3".isInstanceOf[CharSequence])

    val g: Any = "Ala ma kota"
    val h: String = g.asInstanceOf[String] // Cast
    println(decide(3))
    println(decide("Hello"))
  }
}

object ParameterizedTypesOnMethods {
  def decide(b: Boolean, x: Any, y: Any): Any = if (b) x else y

  def decideParameterized[T](b: Boolean, x: T, y: T): T = if (b) x else y

  def main(args: Array[String]): Unit = {
    println(decide(true, "A", "B")) // Any
    println(decide(false, 3, 10)) // Any
    println(decide(true, 'c', 'd')) //Any

    val parameterized = decideParameterized(true, "A", "B")
    println(s"${parameterized}  : ${parameterized.getClass}") // return param String

    val parameterized1 = decideParameterized(false, 3, 10)
    println(s"${parameterized1}  : ${parameterized1.getClass}") // Int

    val parameterized2 = decideParameterized(true, 'c', 'd')
    println(s"${parameterized2}  : ${parameterized2.getClass}") // Char

    val parameterized3 = decideParameterized(true, 4.0, 5.0)
    println(s"${parameterized3}  : ${parameterized3.getClass}")

    val parameterized4 = decideParameterized(true, 3.0, "G") // Any
    println(s"${parameterized4}  : ${parameterized4.getClass}")

  }
}


// Collections
object CollectionOperations {



  def main(args: Array[String]): Unit = {
    val ages = List[Int](2, 52, 34, 9, 24, 67, 12, 18, 29, 3, 6, 88, 71)
    // group by takes as param function which takes elements of the
    // colection (Int in this example) and produces key to be used to group elements into
    // map of one to many relationships
    val grouped: Map[String, List[Int]] = ages.groupBy[String] { age =>
      if (age >= 18 && age < 65) "adult";
      else if (age < 18) "child"
      else "senior"
    }
    println(grouped)
    val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

    // index of language => some score
    val vs : Iterable[(Int,Int)] = List((0,2345),(1,1863),(2,1345),/*(4,1445),*/(0,244),(1,566),(0,457))
    // Map + groupBy + mapValues
    val frequencies : Map[Int,Int]  = vs.map(pair => pair._1).groupBy(identity).mapValues(_.size)
    val sortedFrequencies : Seq[(Int,Int)] = frequencies.toSeq.sortWith(_._2 > _._2)
    //  println(langs(sortedFrequencies.head._1))

      val langLabel: String   = langs(sortedFrequencies.head._1)// most common language in the cluster
      val langPercent: Double = (sortedFrequencies.head._2.toDouble / vs.size) * 100 // percent of the questions in the most common language
      val clusterSize: Int    = vs.size

      println(s"langLabel  : ${langLabel}")
      println(s"langPercent: ${langPercent}")
      println(s"clusterSize: ${clusterSize}")


  }

}


// Abstract class
abstract class Person2 {
  def firstName:String // no implementation - no need for abstract keyword
  def lastName:String
}

// @BeanProperty adds java style  setters an getters
// Primary constructor
class Employee(@BeanProperty var firstName:String,@BeanProperty val lastName: String, val  title: String = "Mr.") extends Person2 {

  // This is part of primary constructor !!!  Precondition checks using  require
  require(firstName.nonEmpty,"First name cannot not be empty!")
  require(lastName.nonEmpty,"Last name cannot not be empty!")
  require(title.nonEmpty,"Title cannot not be empty!")

  // Ancillary constructors
  // typical form
  def this(firstName:String,lastName:String) = this(firstName,lastName,"Mr.") // 2nd constructor
  // Atypical forms
 //def this(firstName:String,lastName:String) = {this(firstName,lastName,"Mr.")}
  //def this(firstName:String,lastName:String)  {this(firstName,lastName,"Mr.");println("Side Effect")}
  // primary constructor has most parameters !

  def fullName = s"$firstName $lastName"

  def changeLastName(ln:String) = new Employee(firstName,ln)

  // Copy method with defaults
  def copy(firstName:String = this.firstName,lastName:String = this.lastName,title:String = this.title) = new Employee(firstName,lastName,title)


  override def equals(obj: scala.Any): Boolean = {

    if(!obj.isInstanceOf[Employee])
      false
    else {
      val other = obj.asInstanceOf[Employee]
      other.firstName == this.firstName && other.lastName == this.lastName && other.title == this.title // == is equals() !!!
    }
  }

  override def hashCode(): Int = {
    var result = 19
    result= 31 * result + firstName.hashCode
    result= 31 * result + lastName.hashCode
    result= 31 * result + title.hashCode
    result
  }

  override def toString: String = s"Employee($firstName,$lastName,$title)"
}

// override in case class
case class Department(name:String) {

  override def toString: String = s"Department: $name"

}

// // Case class cannot be subclassed by cae class
//Error:(874, 12) case class MyDepartment has case ancestor Department, but case-to-case inheritance is prohibited. To overcome this limitation, use extractors to pattern match on non-leaf nodes.
//case class MyDepartment(override val name:String) extends Department(name)  {
//
//}



// Manager extends Employye
class Manager(firstName:String,latsName:String,title:String,val department: Department) extends Employee(firstName,latsName,title) {

  // overiding fullName from Employee
  override def fullName = s"$firstName $lastName, ${department.name}"

  // override copy
//  override def copy(firstName:String = this.firstName,lastName:String = this.lastName,title:String = this.title) = new Manager(firstName,lastName,title, new Department("Toys"))

  // overload copy
 // def copy(firstName:String = this.firstName,lastName:String = this.lastName,title:String = this.title,department: Department = this.department ) = new Manager(firstName,lastName,title, department)
//  Error:(850, 7) in class Manager, multiple overloaded alternatives of method copy define default arguments.
//    The members with defaults are defined in class Manager and class Employee.

}


object ScalaClassess {
  def main(args: Array[String]): Unit = {
    val employee = new Employee("Tomasz","Ignasiak")
    println(employee.firstName)
    println(employee.lastName)
    employee.firstName = "Tomasz Wlodzimierz"
    println(employee.getFirstName)
    println(employee.getLastName)
    employee.setFirstName("Tomasz")
    println(employee.getFirstName)
    println(employee.getLastName)
    //employee.getFirstName
    val employee2 = new Employee(lastName =  "Ritchie", firstName = "Dennis")
    println(employee.fullName)
    println(employee2.fullName)

    val employee3 = employee2.changeLastName("Sosnowski")
    println(employee3.fullName)

    val employee4 = employee2.copy(lastName = "Sosnowski")
    println(employee4.fullName)

    try {
      val employee5 = new Employee("Tomasz","")
    } catch  {
      case ies:IllegalArgumentException => println(ies.getMessage) // pttter matching on type
    } finally {
      println("Continuing ... ")
    }

    val mathematics = new Department("Mathematics")
    val alan:Manager = new Manager("Alan","Turing","Matematician, Logitian",mathematics)

    println(alan.fullName)
    //val alanEmployee:Employee  = alan
    //println(alanEmployee.)

    val otherEmployee = new Employee("Tomasz","Ignasiak")
    println(employee == otherEmployee) // uses equal not object reference lie java
    println(employee eq  otherEmployee) // reference equaity !!!
    println(employee eq employee)


    println(employee.hashCode == otherEmployee.hashCode)
    println(employee)
    println(mathematics)

    val anotherMathematics = new Department("Mathematics")
    println(mathematics == anotherMathematics)
    println(mathematics.hashCode == mathematics.hashCode)

    val phisics = Department("Phisics") /// apply defined to crete

//    val deps = List(mathematics,phisics)

    // pattern maching on case class possible because unapply is automatically implemented
    val name = mathematics match {
      case Department(n) => n
      case _ => "Unknown"
    }

    println(name)

    // // Case class cannot be subclassed by cae class
    //Error:(874, 12) case class MyDepartment has case ancestor Department, but case-to-case inheritance is prohibited. To overcome this limitation, use extractors to pattern match on non-leaf nodes.
//
//    val biologyD = new Department("Biology")
//    val biologyMD = new MyDepartment("Biology")
//    println(biologyD)
//    println(biologyMD)
//    println(biologyD == biologyMD)
//    println(biologyD.hashCode == biologyMD.hashCode)
  }
}

//Parameterized class
case class Box[T](t:T) {
  // Parameterized Method In Class
  def coupleWth[U](u:U):Box[Couple[T,U]] = Box(Couple(t,u))
}

case class Couple[A,B](first:A,second:B) {
  def swap:Couple[B,A] = Couple(second,first)
}

object ParametrizedTypes {
  def main(args: Array[String]): Unit = {
    val intBox:Box[Int] = new Box(2)
    val strBox:Box[String] = new Box("Boxed")
    println(intBox.t)
    println(strBox.t)
    val manBox = new Box[Manager](new Manager("Alan","Price","Mr.",new Department("AAAA")))
    println(manBox.t)
    val coupleIntStr = new Couple[Int,String](23,"ASSSS")

    val intBox2 = Box(200)
    val intBoxCoupledWithString:Box[Couple[Int,String]] = intBox2.coupleWth("Scala")
    println(intBoxCoupledWithString.t.first)
    println(intBoxCoupledWithString.t.second)
    println(intBoxCoupledWithString.t.swap.first)
    println(intBoxCoupledWithString.t.swap.second)
  }
}


// object - singleton. Object cannot be inherited
object MyObject {
  def foo(x:Int,y:Int) = x + y
}

// But it can be inherited from class
object Knuth extends  Employee("Donald","Knuth","Programmer")

object Objects {
  def main(args: Array[String]): Unit = {
    println(MyObject.foo(1,2)) // object method call
    println(Knuth.fullName)
  }
}

// no need for main when object extending App
object RunnerApp extends App{
  println(MyObject.foo(1,2)) // object method call
  println(Knuth.fullName)
}

class SecretAgent(val name:String) {
  def shoot(n:Int): Unit = {
    import SecretAgent._
    decrementBullets(n)
  }
}
object SecretAgent{
  private var b:Int = 3000
  private def decrementBullets(count:Int): Unit = {
    if (b -count <= 0) b = 0
    else b = b - count
  }
  def bullets = b
}

object SecretAgentRunner extends  App {
  val bond = new SecretAgent("James Bond")
  val bourne = new SecretAgent("Jason Bourne")
  bond.shoot(1000)
  println(SecretAgent.bullets)
  bourne.shoot(500)
  println(SecretAgent.bullets)
}

class Hero(val name: String, private val superheroName: String) // superheroName not accessible to outside world

object Hero {
  def showInnerSeret(h:Hero) = h.superheroName
}

object HeroRunner extends App {
  val clark = new Hero("Clark Kent","Superman")
  println(Hero.showInnerSeret(clark))
}

// private constructor or protected (accessible by test) !!!
class EmployeeHD protected/*private*/ (firstName:String,lastName: String, title: String, val hireDate: LocalDate) extends Employee(firstName,lastName,title)  {

}

object EmployeeHD {
  // factory method
  def /*create*/apply(firstName:String,lastName: String, title: String) = new EmployeeHD(firstName,lastName,title,LocalDate.now)
  def /*create*/apply(firstName:String,lastName: String, title: String,hireDate: LocalDate) = new EmployeeHD(firstName,lastName,title,hireDate)
}

object EmployeeHDRunner extends App {
  val employee = EmployeeHD/*.create*/("Tomasz","Ignasiak","Programmer") // calls apply method
  println(employee.hireDate)
  val employee2 = EmployeeHD/*.create*/("Tomasz","Ignasiak","Programmer",LocalDate.of(2007,7,1)) // calls apply method
  println(employee2.hireDate)
}

class Foo(x:Int) {
  def apply(y:Int) = x +y
}

object MagicApply extends  App {
  val foo = new Foo(10)
  println(foo(20)) // for apply you do not need to call method
}


// http://blog.bruchez.name/2011/10/scala-partial-functions-without-phd.html
object PartialFunctions {

  val fraction = new PartialFunction[Int, Int] {
    def apply(d: Int) = 42 / d

    def isDefinedAt(d: Int) = d != 0
  }

  val fraction2: PartialFunction[Int, Int] = {
    case d: Int if d != 0 ⇒ 42 / d
  }

  val incAny: PartialFunction[Any, Int] = {
    case i: Int ⇒ i + 1
  }

  val liar: PartialFunction[Any, Int] = {
    case i: Int ⇒ i;
    case s: String ⇒ s.toInt
  }

  def isParsableAsInt(s: String) = {
    try {
      Integer.valueOf(s)
    } catch {
      case nfe: NumberFormatException =>  false
      case _ =>  true
    }
    true
  }

  val honest: PartialFunction[Any, Int] = {
    case i: Int ⇒ i;
    case s: String if isParsableAsInt(s) ⇒ s.toInt
  }

  def main(args: Array[String]): Unit = {
    fraction(42)
    //fraction(0) // divide by zero exception

    println(fraction2.isDefinedAt(42))
    println(fraction2.isDefinedAt(0))

    fraction2(42)
    //fraction2(0) // Match error exception

    println(incAny.isDefinedAt(41))
    println(incAny.isDefinedAt("cat"))

    // List(41, "cat") collect { case i: Int ⇒ i + 1 }
    println(List(41, "cat") collect { case i: Int ⇒ i + 1 }) // it does not fail because collect accepst partial funtion
    //println( List(41, "cat") map { case i: Int ⇒ i + 1 }) // it fails with match erro becau
    liar.isDefinedAt("cat") // says true
    //liar("cat") but fails with number format exception

    val pets = List("cat", "dog", "frog") //
    println(pets.isDefinedAt(0))
    println(pets.isDefinedAt(3))
    println(Seq(1, 2, 42) collect pets)
    println(pets.lift(3)) // lifts funtion from partial to "full" returning Option instead of value
  }
}

class HigherOderFunctions {

  def totalPrices(prices: List[Int], selector: Int => Boolean) = {
    var total = 0
    for (price <- prices) {
      if (selector(price))
        total += price
    }
    total
  }
}

object HigherOderFunctions {
  def main(args: Array[String]) {

    val prices = List(1, 4, 56, 33, 677, 123, 45, 78, 33, 76, 12, 44, 23, 9, 8, 66, 4, 7, 8)
    println(new HigherOderFunctions().totalPrices(prices, { price => true })) // all
    println(new HigherOderFunctions().totalPrices(prices, { price => price < 25 })) // all < 25
    println(new HigherOderFunctions().totalPrices(prices, { price => price >= 25 })) // all >= 25
  }
}


// Decorator Pattern with Traits
abstract class Writer {
  def write(msg: String)
}

class StringWriter extends Writer {
  val target = new StringBuilder

  def write(msg: String) = target.append(msg)

  override def toString() = target.toString()
}

// extends is more like contraint then the inheritence
trait UpperCaseFilter extends Writer {
  abstract override def write(msg: String) = {
    super.write(msg.toUpperCase)
  }
}

trait ProfanityFilter extends Writer {
  abstract override def write(msg: String) = {
    super.write(msg.replace("stupid", "s*****"))
  }
}


object DecoratorByChainingTraits {
  def writeStuff(writer: Writer) = {
    writer.write("This is stupid")
    println(writer)
  }


  def main(args: Array[String]) {
    writeStuff(new StringWriter)
    writeStuff(new StringWriter with UpperCaseFilter)
    writeStuff(new StringWriter with ProfanityFilter)
    writeStuff(new StringWriter with UpperCaseFilter with ProfanityFilter)
    writeStuff(new StringWriter with ProfanityFilter with UpperCaseFilter) ///profanity filter does not work
    // because UpperCaseFilter is applayed first Filters are applied from right to left !!!
  }
}


object StringUtilV1 {
  def joiner(strings: String*): String = strings.mkString("-")

  def joiner(strings: List[String]): String = {
    joiner(strings: _*)
  }

  //
  //COMPILATION ERROR
  def main(args: Array[String]): Unit = {
    println(StringUtilV1.joiner(List("Programming", "Scala")))

  }
}

object Playing {
  def play(): Unit = {
    // Multiline string
    val str: String =
      """ Ala ma kota,
        |  a kot ma Ale
        |    ale kot lubi myszy
      """.stripMargin
    println(str /*.stripMargin*/)
    //funtions literals f1 is exacly the sam as f2
    val f1: (Int, String) => String = (i, s) => s + i
    val f2: Function2[Int, String, String] = (i, s) => s + i

    // tuple literals - they are the same
    val t1: (Int, String) = (1, "two")
    val t2: Tuple2[Int, String] = (1, "two")
    println(t1)
    println(t2)

    // Tuple3
    val t = ("Hello", 1, 2.3)
    println(t)
    println(t._1)
    println(t._2)
    println(t._3)

    // Tuple3 where elements acessible as separate values
    val (tf1, tf2, tf3) = ("World", '!', 0x22)
    println(tf1)
    println(tf2)
    println(tf3)

    def main(args: Array[String]) {
      play()
    }
  }
}


case class Pers(val firstName:String , val middleName:Option[String], val lastName:String) {
  def this(firstName:String ,middleName:String, lastName:String) = this(firstName,Some(middleName),lastName)
  def this (firstName:String ,lastName:String) = this (firstName,None,lastName)
  def this() = this("Unknown","Unknown")
}
object Options extends  App {
  val middleName = Some("Antony")
  val middleName2:Option[String] = middleName
  val noMiddleName = None
  val middleName3:Option[String] = noMiddleName
  val middleName4:Option[Nothing] = noMiddleName // Nothing is subtype of any class
  val middleName5:None.type = noMiddleName

  val tomasz = new Pers("Tomasz","Ignasiak")
  println(tomasz)
  val tomasz2 = new Pers("Tomasz","Wlodzimierz","Ignasiak")
  println(tomasz2)
  val  strange = new Pers
  println(strange)

  println(tomasz.middleName.getOrElse("No middle Name"))
  println(tomasz2.middleName.getOrElse("No middle Name")) // get would return error
  println(peelMidlleName(tomasz.middleName))
  println(peelMidlleName(tomasz2.middleName))
  println(peelMidlleName(strange.middleName))

  def peelMidlleName(x:Option[String]):String = {
    x match {
      case Some(name) => name
      case None => "No middle name"
    }
  }
}

object OptionSomeNone {

  def main(args: Array[String]) {
    val stateCapitals = Map(
      "Alabama" -> "Montgomery",
      "Alaska" -> "Juneau",
      // ...
      "Wyoming" -> "Cheyenne")
    println("Get the capitals wrapped in Options:")
    println("Alabama: " + stateCapitals.get("Alabama"))
    println("Wyoming: " + stateCapitals.get("Wyoming"))
    println("Unknown: " + stateCapitals.get("Unknown"))
    println("Get the capitals themselves out of the Options:")
    println("Alabama: " + stateCapitals.get("Alabama").get)
    println("Wyoming: " + getCapital(stateCapitals, "Wyoming"))
    println("Unknown: " + getCapital(stateCapitals, "Unknown"))
    //
    //    val capital: String = getCapital(stateCapitals)
    //    println(capital);

  }

  // Pattrn maching on Some/None
  def getCapital(stateCapitals: Map[String, String], state: String): String = {
    val capitalO: Option[String] = stateCapitals.get(state)
    val capital = capitalO match {
      case Some(capital) => capitalO.getOrElse("Oops!")
      case None => "Not defined !!!"
    }
    capital
  }
}

object Tuple extends App {
  val t = (1,"Cool",403.00)
  println(s"(${t._1},${t._2},${t._3})")
  val t1:(Int,String,Double) = t
  val t2:Tuple3[Int,String,Double] = t
  println((1.0,"2").swap)
}

object Funtions extends App {

  // Function0 0 input params + one output param
  //  val f0:Function0[Int] = new Function0[Int] {
  //    override def apply(): Int = 1
  //  }

  // same as above () => Int funtion which takes no ars and returns Int after = body no params returns 1
  val f0/*:() => Int*/ = () => 1


  /*
  val f1:Function1[Int,Int] = new Function[Int,Int] {
    override def apply(x: Int): Int = x + 1
  }
*/

  // same as above
  //val f1:(Int => Int)  = (x:Int) => x +1
  val f1/*:Int => Int */ = (x:Int) => x +1

//
//  val f2:Function2[Int,String,String] = new Function2[Int,String,String] {
//    override def apply(v1: Int, v2: String): String = v2 + v1
//  }

  // type declaration optional
  val f2/*:(Int,String) => String*/ = (v1:Int,v2:String)  => v2 + v1

  // Fuction which takes string as param  and returns Tuple(String,Int) = Body (parma list
  val f3/*: String => (String,Int)*/ = (x:String) => (x,x.size)
  // same as above without funtion typ declaration )inferred


     // In java 8 Funtion0 -> Funtion22 are gone because they are compiled to  java.util.Funtion

  println(f0)
  println(f1.apply(3))
  println(f1(5))
  println(f2(3,"WOW"))
  println(f3("laser"))

}


object MyFObject {
  val f = (x:Int) => x +1 // function
  def g(x:Int) = x + 1 // method

}

class Foo2(x:Int) {
  def bar(y:Int) = x + y
  def gym(z:Int,a:Int) = x + z + a
}

class Baz(z:Int) {
  def qux(f:Int => Int) = f(z)
  def jam(f:(Int,Int) => Int) = f(z,10)
}

object MethodOrFuntion extends App {
  println(MyFObject.f.apply(4))
  println(MyFObject.g(4)) // method ust be applied in contect of object class (here static)


  val x = new Foo2(10)
  // partially applie funtion
  val fx = x.bar(_) // method converted to fuction (assigned to val/var) takes x from costructor becoming 10 + y function
  println(fx.apply(20))
  println(fx(20))

  val z = new Baz(20)
  println(z.qux(fx))
  // inlined funtion
  println(z.qux(x.bar _))
  println(z.qux(x.bar)) // if _ is last param in method we can omit it - bassicaly it looks like we passing to qux method instead of funtion

  // we create partialy applyied function, x s taken from Foo2 constructor, we give z = 40 but for a param we say =we don't know value in this moment
  // thus we reducing funtion to Int => Int
  val fx2 = x.gym(40,_:Int)

  println(z.qux(fx2))

  val fx3  = x.gym _

  println(z.jam(fx3))
  // the same as above inlined  x.gym(_) call
  println(z.jam(x.gym))
}

class Foo3(x:Int) {
  def bar(y:Int => Int) = y(x)
 // def gym(z:Int,a:Int) = x + z + a
}

object Closures extends  App {
  val m  = 200
  // m is comming from outside of the function. it soul be immmutable !
  val f  = (x:Int) => x + m
  val foo = new Foo3(100)
  println(foo.bar(f))
}


object FunctionsWithFunctions extends  App {
 val f/*:(Int,Int => Int) => Int*/ = (x:Int,y:Int =>Int) => y(x)
  println(f(3,/*m => m + 1*/ _ + 1))
  println(f(3,/*m => m + m*/ _ * 2))
  println(f(3,m => m * m))// how to do that with _ ?

  val g = (x:Int) => (y:Int) => x + y // funtion which takes int and returns function taking y as param, use of x makes this function closure

  println(g(4)) // return function
  println(g(4)(5))  // returns value from execution of the funtion x =4 y = 5
  println(g.apply(4).apply(5))

}


object Currying extends App {
  val g = (x:Int) => (y:Int) => x + y // returns function which return sum
  val f = (x:Int,y:Int) => x + y  // return sum
  val fc = f.curried // we took function returning sum and curried it so it returns the function returning sum like g
  printType(g(7))
  printType(fc(7))

  println(g(7)(5))
  println(f(7,5))
  println(fc(7)(5))
  val fbis = Function.uncurried(fc) // uncurying funtion fc
  println(fbis(7,5))

  def foo(x:Int,y:Int,z:Int) = x + y + z
  def bar(x:Int)(y:Int)(z:Int) = x + y + z
  def baz(x:Int,y:Int)(z:Int) = x + y + z



  import reflect.runtime.universe._

  implicit class ColonTypeExtender [T : TypeTag] (x : T) {
    def colonType = typeOf[T].toString
  }

  def printType[T : TypeTag] (x : T) = println(s"${x.colonType} = ${x}")

  val f2 = foo _ // (Int, Int, Int) => Int = <function3>
  printType(f2)
  val b = bar _ // Int => (Int => (Int => Int)) = <function1>
  printType(b)
  val c = baz _ // (Int, Int) => Int => Int = <function2>
  printType(c)

  val f3 = foo(5,_:Int,_:Int)
  printType(f3)

}

object ByNameParameters extends App {
  def byValue(x:Int)(y:Int) =  {println("By Value:"); x + y}
  def byFuntion(x:Int)(y: () => Int) = {println("By Function:"); x + y() }
  def byName(x:Int)(y: => Int) = {println("By Name:"); x + y }

  val a = byValue(3)/*(19) */{
    println("In call");19
  } // eager  + simple
  val b = byFuntion(3){
    () => println("In call");19
  } // lazy + complex (funtion passed)
  val c = byName(3){
    println("In call");19
  } // lazy and easy


  // Can be calld by the block
  // Good for caching exceptions and cleaning
  // by name param its funtion but its treated like normal val
  def divideSafeLy(f: => Int) : Option[Int] = {
    try {
      Some(f)
    } catch {
      case ae:ArithmeticException => None
    }
  }


  val d = divideSafeLy {
    val x = 10
    val y = 5
    x/y
  }

  println(d)

  val e = divideSafeLy {
    val x = 10
    val y = 0
    x/y
  }

  println(e)

}

object Lists extends  App {
  val a = List(1,2,3,4,5)
  val a2 = List.apply(1,2,3,4,5) // what is accually called apply on companion object
  val a3 = 1 :: 2 :: 3 :: 4 :: 5 :: Nil // Nill is empty list :: - right associative colon 5 :: Nill = Nill.::(5)

  println(a.head)// 1
  println(a.tail)// 2,3,4,5
  println(a.init)// 1,2,3,4
  println(a.last) // 5
  println(a.apply(4)) // a.apply(4) -- apply on class in this case
  println(a.min)
  println(a.max)
  println(a.updated(3,100)) // returns opy of the list with 3rd element replaced with 100
  println(a.mkString(","))
  println(a.mkString("*"))
  println(a.mkString("[",",","]"))
}

object Sets extends  App {

  val set = Set(1,2,2,2,3,4,4,5,5,6)
  println(set) /// duplicates removed unordered
  val set2 = Set(1,2)
  println(set diff set2) // existing in set but not in set2
  println(set2 diff set) // existing in set2 but not in set - empty
  println(set union set2) //sum of sets
  println(set intersect set2) // common part of sets

  println(set ++ set2) // same as union but works with ther collections

  println(set ++ List(12,7,9))
  println(set -- set2) // subratract collection
  println(set - 2) // subratract element
  println(set.apply(4)) // = contains
  println(set.contains(4)) // = contains
  println(set(4)) // = contains
}

object Maps extends  App {
  val m = Map.apply((1,"One"),(2,"Two"),(3,"Three"))
  val m2 = Map((1,"One"),(2,"Two"),(3,"Three")) // same
  val m3 = Map(1 -> "One",2 -> "Two",3 -> "Three") // -> method creates touples from self/this (key) and param (value)
  println(m3.get(1)) // Some("One")
  println(m3(1)) // apply() call : One
  println(m3.get(4)) // None
 // println(m3(4)) // Error

  println(m3.toList) // List of Tuples
  println(m3.toSet)
  println(m3.keys) // Set
  println(m3.keySet) // Set
  println(m3.values) // MapLike
  println(m3.values.toSet)

  val s = new String("Co")
  val s2 = "Co" // interned
  println(s == s2) // true (comparision of properties)
  println(s eq s2) // false  (comparision of references)

  val co = Symbol("Co") // Some always interned
  val co2 = 'Co // same as above 'String notation - interned
  println(co == co2) // true (comparision of properties)
  println(co eq co2) // true  (comparision of references)

 // Symbols are perfect for map keys !!!

  val elements: Map[Symbol,String] = Map('Co -> "Cobalt", 'H -> "Hellium", 'Pb -> "Lead")
  println(elements)
  println(elements.get('Co)) // Some(Cobalt)


}

object Arrays extends  App {
  val a:Array[Int] = Array(1,2,3,4,5) // int[]
  println(a)

  println(a.head)// 1
  println(a.tail)// 2,3,4,5 int[]
  println(a.init)// 1,2,3,4 int[]
  println(a.last) // 5
  println(a.apply(4)) // a.apply(4) -- apply on class in this case
  println(a(4)) // a.apply(4) -- apply on class in this case
  println(a.min)
  println(a.max)
  println(a.isEmpty)

  def repeatedParamMethod(x:Int, y:String, z:Any*) = {
    println(z)// WrappedArray
    s"${x} ${y} give you ${z.mkString(", ")}"
  }

 println(repeatedParamMethod(3, "eggs", "a delitious sanwitch","protein"))
  println(repeatedParamMethod(3, "eggs", List("a delitious sanwitch","protein")))
  println(repeatedParamMethod(3, "eggs", List("a delitious sanwitch","protein"):_*)) // :_* - converts list to vararg/repeated param

}

object Ranges extends  App {
  var r = 1 to 10 // include 10
  var r2 = 1 until  10 // exclude 10
  println(r)
  println(r2)
  println(r2.head)
  println(r2.tail)

  val r3 = 2 to 10 by 2
  println(r3)
  val r4 = 10 to 2 by -2
  println(r4)
  val r5 = 'a' to 'z' // NumericRange
  println(r5)
  val r6:IndexedSeq[Char] = r5 ++ ('A' to 'Z') // Vector
  println(r6)
  val r7 = Range.inclusive(1,10)
  println(r7)
}

object MapFunction extends  App {

  val a = List(1,2,3,4,5,6)
  val f = (x:Int) => x + 1
  println(a.map(f))
  println(a.map((x:Int) => x + 1 ))
  println(a.map(x => x + 1 ))
  println(a.map(_ + 1 ))
  println(a.map(1  + _))
  println(a.map(1+))// postfix operator
  val b = Set("Brown","Red","Green","Purple","Gray","Yellow")
  println(b.map(x=> x.size))
  println(b.map(_.size))
  println(b.map(x=> (x,x.size)))
  //println(b.map{ case: (_,_.size)})

  val fifaChamps = Map("Germany" -> 4, "Brazil" -> 5, "Italy" -> 4,"Argentina" -> 2)
  println(fifaChamps.map(t=> ("Team " + t._1,t._2)))

}

object FilterFunctions extends  App {

  val a = 1 to 10
  println(a.filter(x=> x % 2 == 0))
  println(a.filter(_ % 2 == 0))
  println(a.filterNot(_ % 2 == 0))
  println(a.exists(_ % 2 == 0))

  def filterVowels(s:String) = s.toLowerCase.filter(c => Set('a','e','i','o','u').contains(c))
  println(filterVowels("Orange"))

  val b = Set("Brown","Red","Green","Purple","Gray","Yellow")
  println(b.filter(s => filterVowels(s).size > 1)) // filter colors with 1 or more vowels

  val m = Map(1 -> "One",2 -> "Two",3 -> "Three",4 -> "Four")
  println(m.filter(_._1% 2 == 0))

  println(Some(5).filter(_ % 2 == 0)) // None
  println(Some(4).filter(_ % 2 == 0)) // Some(4)
}

object Foreach extends  App {
  val a = 1 to 10
  a.foreach(x => println(x))
  a.foreach(println(_))
  a.foreach(println _)
  a.foreach(println)
  a foreach println
}


object FlatMapFunction extends  App {
  val a = List(1,2,3,4,5)
  println(a.map(x => List(-x,0,x))) // Int => List
  println(a.map(x => List(-x,0,x)).flatten) // flatten map = flatMap
  println(a.flatMap(x => List(-x,0,x)))

  val b:List[List[List[Int]]] = List(List(List(1,2,3),List(4,5,6)),List(List(7,8,9),List(10,11,12)))
  println(b.flatMap(c=>c))
  println(b.flatMap(c=>c).flatMap(c=>c))

  println(Set(2,4,6).flatMap(x => Set(x,x *2)))
  val m = Map(1 -> "One",2 -> "Two",3 -> "Three",4 -> "Four")
  println(m.flatMap(t => Map(t._1 -> t._2,(t._1 * 100) -> (t._2 + " Hunderd"))))

  println(Some(14).flatMap(x => Some(x + 19)))
  println(None.asInstanceOf[Option[Int]].flatMap(x => Some(x + 19)))

}

object ForComprehensions extends  App {
  for(i <- 1 to 10) {
    println(i)
  }
  val result = for (i <- 1 to 10) yield (i + 1) // like map
  println(result)
  val result2 =  (1 to 10).map(_ + 1) // like map
  println(result2)
  val result3 = for (i <- Some(100)) yield (i + 40) // some
  println(result3)
  val result4 = Some(100).map(i => i + 40)
  println(result3)
  val result5 = for(i <- List(1,2,3,4); j <- List(5,6,7,8)) yield (i,j) // like nested for loop
  println(result5)
  val result6 =  List(1,2,3,4).map(i => List(5,6,7,8).map(j => (i,j)))
  println(result6) // List(List(
  val result7 =  List(1,2,3,4).flatMap(i => List(5,6,7,8).map(j => (i,j))) // same as result5
  println(result7)
  val result8 = for(i <- List(1,2,3,4) if (i % 2) == 0;
                    j <- List(5,6,7,8)) yield (i,j)
  println(result8)

  // Same as above for comprehension using filter/flatmap/map
  val result9 = List(1,2,3,4)
                .filter(i => i % 2 == 0)
                .flatMap(i => List(5,6,7,8)
                  .map(j=> (i,j)))

  println(result9)

  // replacing filter with withFilter which is lazy (more performant ?)
  val result10 = List(1,2,3,4)
                  .withFilter(i => i % 2 == 0)
                  .flatMap(i => List(5,6,7,8)
                    .map(j=> (i,j)))

  println(result10)
}



object ForComprehensions2 {
  def main(args: Array[String]) {
    val dogBreeds = List("Doberman", "Yorkshire Terrier", "Dachshund", "Scottish Terrier", "Great Dane", "Portuguese Water Dog")
    for (breed <- dogBreeds) // = Java 'for ( breed : dogBreeds)'
      println(breed)
    // <- arrow operator is known as generator expression
    for (i <- 1 to 10)
      println(i)

    // Filtering Values
    for (breed <- dogBreeds if breed.contains("Terrier") && !breed.startsWith("Yorkshire"))
      println(breed)

    // for {} yield or for() yield are used if result of filtering should bee assigned to value or variable
    val filteredBreeds = for {
      breed <- dogBreeds
      if breed.contains("Terrier") && !breed.startsWith("Yorkshire")
    } yield breed

    println(filteredBreeds)
    // println(filteredBreeds.getClass)


    // In comprehensions  yo can define
    //    values inside the first part of your for expressions that can be used in the
    //    later expressions,
    for {
      breed <- dogBreeds
      upcasedBreed = breed.toUpperCase()
    } println(upcasedBreed)


    // Option(s) i.e. Some None behaves like collections with 1 value - we can also use  for comprehensions on them
    val dogBreedsUsingOptions = List(Some("Doberman"), None, Some("Yorkshire Terrier"),
      Some("Dachshund"), None, Some("Scottish Terrier"),
      None, Some("Great Dane"), Some("Portuguese Water Dog"))

    println("first pass:")
    for {
      breedOption <- dogBreedsUsingOptions
      breed <- breedOption
      upcasedBreed = breed.toUpperCase()
    } println(upcasedBreed)
    println("second pass:")
    //    Better version  using pattern
    //      matching. The expression Some(breed) <- dogBreeds only succeeds
    //    when the breedOption is a Some
    for {
      Some(breed) <- dogBreedsUsingOptions
      upcasedBreed = breed.toUpperCase()
    } println(upcasedBreed)
  }
}

object FoldAndReduce extends  App {
  println("foldLeft (1 - 10)")
  val foldLeftResult = (1 to 10).foldLeft(0)/* Seed */{(total:Int,next:Int) =>
    println(s"Total: $total, Next: $next")
    total + next/*Acumulating Function  */}
  println(foldLeftResult)
  println("reduceLeft (1 - 10)")
  val reduceLeftResult = (1 to 10).reduceLeft/* No Seed - leftmost element is becoing seed */{(total:Int,next:Int) =>
    println(s"Total: $total, Next: $next")
    total + next/*Acumulating Function  */}
  println(reduceLeftResult)
  println("foldRight (1 - 10)")
  val foldRighResult = (1 to 10).foldRight(0)/* Seed */{(next:Int,total:Int) => // total second !
    println(s"Total: $total, Next: $next")
    total + next/*Acumulating Function  */}
  println(foldRighResult)
  val reduceRightResult = (1 to 10).reduceRight/* No Seed - leftmost element is becoing seed */{(next:Int,total:Int) =>
    println(s"Total: $total, Next: $next")
    total + next/*Acumulating Function  */}
  println(reduceRightResult)

  println((1 to 10).sum) // +
  println((1 to 10).product) // *

  println((1 to 10).foldLeft(0)(_ /*Total*/ + _ /* Next */)) // shortcut
  println((1 to 10).reduceLeft(_ + _))  // shortcut
}

object Zip extends  App {
  val a =  List(1,2,3,4)
  val b =  List(5,6,7,8)
  println(a zip b) // list of tuples !!!
  println((1 to 5) zip (6 to 9)) // different sizes - size dropped
  println((1 to 2) zip (6 to 9)) //Always size if shorter collectin !
}

object Funtional extends  App {
  val groceries = List("Apples","Milk","Naan","Eggs","Oranges","Almonds","Peanut Butter")
  //
  groceries.zipWithIndex./*map(p => (p._2 + 1,p._1))*/foreach{ t => println(s"${t._2 + 1}.${t._1}")}
  groceries.zipWithIndex.map(p => (p._1,p._2+ 1)).map(t => t.swap).foreach{ t => println(s"${t._1}.${t._2}")}


  // Using view will make it lazy so nothing will be evalated here
  val computation = groceries.view.zipWithIndex.map(p => (p._1,p._2+ 1)).map(t => t.swap).map( t => s"${t._1}.${t._2}")
// only heare whole computation  will be executed (mkString)
  println(computation.mkString("\n"))


}

object Srqt extends  App {

  def improve(guess: Double, x: Double): Double = guess + x / 2

  def isGoodEnough(guess: Double, x: Double): Boolean = ???

  @tailrec
  def srqtIter(guess: Double, x: Double): Double = {
    if(isGoodEnough(guess,x)) 
       guess
    else 
      srqtIter(improve(guess,x),x)
  }

  def sqrt(x:Double): Double = srqtIter(x/2,x);
}


// block-structured syntax for declaring package scope- can be used in addition to traditional  java syntax
package com {
  package example {
    package pkg1 {

      class Class11 {
        def m = "m11"
      }

      class Class12 {
        def m = "m12"
      }

    }

    package pkg2 {

      class Class21 {
        def m = "m21"

        def makeClass11 = {
          new pkg1.Class11
        }

        def makeClass12 = {
          new pkg1.Class12
        }
      }

    }

    // “chain” several packages
    package pkg3.pkg31.pkg311 {

      class Class311 {
        def m = "m21"
      }

      object Class311 {
        def main(args: Array[String]) {
          println(s"Class: ${getClass}")
        }
      }

    }

  }
}



//
//package com.example2
//// Bring into scope all package level declarations in "mypkg".
//package mypkg
//
//class MyPkgClass {
//}
//object MyPkgClass {
//  def main(args: Array[String]) {
//    println (s"Class: ${getClass}")
//  }
//}

object ScopedJavaImport {
  def stuffWithBigInteger() = {
    // import java BigInteger and limit scope
    import java.math.BigInteger.{TEN, ZERO => JAVAZERO, ONE => _} // JAVAZERO IS ALIAS !!!
    // println( "ONE: "+ONE ) // ONE is effectively undefined
    println("TEN: " + TEN)
    println("ZERO: " + JAVAZERO)
    // statically imported
    println(s" separatorChar=$separatorChar")

    val javaMap: util.Map[String, String] = new util.HashMap[String, String]()
    javaMap.put("Ala", "Kowalska")
    println(s"${javaMap.get("Ala")}")


    // Package object are good way to expose API
    import com.ti._
    val tradingApi: TradingApi = getTradingApi("IB")
    tradingApi.placeOrder(Order(("EUR", "USD"), 12300.00))

  }

  def main(args: Array[String]) = stuffWithBigInteger()
}


//Parametrized Types and family polymorphism or covariant
// specialization where type of the cnstructor parameter(s)
// depends on specified generic type (In)

// = abstract class BulkReader[In]

object Generics {

  abstract class BulkReader {
    type In
    val source: In

    def read: String // Read source and return a String
  }

  //class StringBulkReader(val source: String) extends BulkReader[String]
  class StringBulkReader(val source: String) extends BulkReader {
    type In = String

    def read: String = source
  }

  // class FileBulkReader(val source: File) extends BulkReader[File] {...}
  class FileBulkReader(val source: File) extends BulkReader {
    type In = File

    def read: String = {
      val in = new BufferedInputStream(new FileInputStream(source))
      val numBytes = in.available()
      val bytes = new Array[Byte](numBytes)
      in.read(bytes, 0, numBytes)
      new String(bytes)
    }
  }

  def main(args: Array[String]) {

    println(new StringBulkReader("Hello Scala!").read)
    // Assumes the current directory is src/main/scala:
    println(new FileBulkReader(
      new File("C:\\Users\\tomasz\\development\\ScalaHelloWord\\src\\TestClassJava.java")).read)
  }
}

object SyntacticSugar {
  def isEven(n: Int) = (n % 2) == 0

  def main(args: Array[String]) {
    List(1, 2, 3, 4) filter isEven foreach println
    // Equals
    List(1, 2, 3, 4).filter(isEven).foreach(println)
  }
}

object RightExpressionBindingAndPrependingElements {
  def main(args: Array[String]) {
    //  Any method whose name ends with a : binds to the right, not the left.
    val list = List('b', 'c', 'd')
    println(list)
    // you can prepend an element to a  List using the :: method, called “cons,” which is short for “constructor,”
    val prependedList = 'a' :: list // = list.::('a')
    println(prependedList)
  }
}

object IfIsExpressionInScala {
  def main(args: Array[String]) {
    //    that if statements and almost all other
    //    statements in Scala are actually expressions that return values. So, we can
    //      assign the result of an if expression, as shown here:
    val configFile = new java.io.File("somefile.txt")

    val configFilePath = if (configFile.exists()) {
      configFile.getAbsolutePath()
    } else {
      configFile.createNewFile()
      configFile.getAbsolutePath()
    }
  }
}

// Rarely Used
object WhileLoops {

  import java.util.Calendar

  def isFridayThirteen(cal: Calendar): Boolean = {
    val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
    val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)
    // Scala returns the result of the last expression in a method
    (dayOfWeek == Calendar.FRIDAY) && (dayOfMonth == 13)
  }


  def main(args: Array[String]) {
    var cal: Calendar = Calendar.getInstance()
    while (!isFridayThirteen(cal)) {

      cal.add(Calendar.DAY_OF_MONTH, 1)
      // isFridayThirteen(Calendar.getInstance())
    }
    println(s"${cal.getTime} is the next Friday the 13th !")


    var count = 0
    do {
      count += 1
      println(count)
    } while (count < 10)
  }

}

//
//Unlike Java, Scala does not have checked exceptions, which are now
//regarded as an unsuccessful design. Java’s checked exceptions are
//treated as unchecked by Scala. There is also no throws clause on
//method declarations. However, there is a @throws annotation that is
//useful for Java interoperability.
object TryCatch {

  /** Usage: scala rounding.TryCatch filename1 filename2 ... */
  def main(args: Array[String]) = {
    //args foreach (arg => countLines(arg))                            // <1>
    countLines("C:\\Users\\tomasz\\development\\ScalaHelloWord\\src\\TestClassJava.java")
  }

  import scala.io.Source
  import scala.util.control.NonFatal

  def countLines(fileName: String) = { // <3>
    //    Declare the source to be an Option, so we can tell in the finally
    //    clause if we have an actual instance or not.

    println() // Add a blank line for legibility
    var source: Option[Source] = None // <4>
    try { // <5>
      // throws java.io.FileNotFoundException
      source = Some(Source.fromFile(fileName)) // <6>
      val size = source.get.getLines.size
      println(s"file $fileName has $size lines")
    } catch {
      //      Scala uses pattern matches to pick the exceptions
      //        you want to catch. This is more compact and more flexible than Java’s use
      //        of separate catch clauses for each exception. In this case, the clause case
      //      NonFatal(ex) => … uses scala.util.control.NonFatal to match any
      //      exception that isn’t considered fatal.
      case NonFatal(ex) => println(s"Non fatal exception! $ex") // <7>
    } finally {
      //      Use a for comprehension to extract the Source instance from the Some
      //      and close it. If source is None, then nothing happens!
      for (s <- source) { // <8>
        println(s"Closing $fileName...")
        s.close
      }
    }
  }
}


object TryCatchARM {


  def main(args: Array[String]) {
    //args foreach (arg => countLines(arg))
    countLines("C:\\Users\\tomasz\\development\\ScalaHelloWord\\src\\TestClassJava.java")
  }

  import scala.io.Source

  def countLines(fileName: String) = {
    println() // Add a blank line for legibility
    // Source is not evaluate immediately, before execution moves to manage.
    //    The evaluation of the
    //      expression is delayed until the line val res = Some(resource) within
    //      manage. This is what the by-name parameter resource
    manage(Source.fromFile(fileName)) { source =>
      val size = source.getLines.size
      println(s"file $fileName has $size lines")
      if (size > 20) throw new RuntimeException("Big file!")
    }
  }


  object manage {

    import scala.util.control.NonFatal

    def apply[R <: {def close() : Unit}, // The <: means R is a subclass of something else, in this case a
    //structural type with a close():Unit method - uses reflection
    // Same as  R <: Closable
    T]
    (resource: => R) // resource is a function with an unusual declaration.
    //    Actually, resource is a by-name parameter. For the moment, think of
    //      this as a function that we call without parentheses
    (f: R => T) = {
      // anonymous function that will take the resource as an
      //  argument and return a result of type T
      var res: Option[R] = None
      try {
        res = Some(resource) // Only reference "resource" once!!
        // resource behaves like a function - evaluation is deffered here - it efectively does:
        // val res = Some(Source.fromFile(fileName))
        f(res.get)
      } catch {
        case NonFatal(ex) => println(s"Non fatal exception! $ex")
      } finally {
        if (res != None) {
          println(s"Closing resource...")
          res.get.close
        }
      }
    }
  }

}

object CallByName {

  // defines while like loop
  @annotation.tailrec // <1>
  def continue(conditional: => Boolean)(body: => Unit) { // <2>
    if (conditional) { // <3>
      body // <4>
      continue(conditional)(body)
    }
  }


  def main(args: Array[String]) {
    var count = 0 // <5>
    continue(count < 5) {
      println(s"at $count")
      count += 1
    }
  }
}

// I finished on page 134 (Lazy val)