import java.util.StringJoiner;

/**
 * Created by tomasz on 1/30/2016.
 */
public class TestClassJava {
    private final String test;
    private final String name;
    private final Integer value;

    public TestClassJava(String test, String name, Integer value) {
        this.test = test;
        this.name = name;
        this.value = value;
    }

    public String getTest() {
        return test;
    }

    public String getName() {
        return name;
    }


    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return new StringJoiner(test).add(name).add(value + "").toString();
    }
}
